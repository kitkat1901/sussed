/**
 * Creates a Typer instance for a given DOM element and allows the typing effect
 * to be encapsulated for the given text or texts.
 * @class
 * @param    {String}   selector        - The element(s) to apply the effect to.
 * @param    {String[]} texts           - The string or (array of strings) to type.
 * @param    {Number}   period          - The period of each tick event of the Typer instance.
 * @property {NodeList} Textani.elements  - The elements returned by the given selector.
 * @property {Number}   Textani.period    - The period of each tick event of the current Typer instance.
 * @property {Number}   Textani.iteration - The number of iterations made.
 * @property {Boolean}  Textani.deleting  - Whether the instance is in the typing or deleting state.
 * @property {String}   Textani.current   - The currently selected string being typed.
 * @property {String[]} Textani.texts     - A list of all texts used in the Typer.
 */
function Textani(selector, texts, period) {
    // Validate constructor parameter.
    if ((typeof(selector)) !== "string") {
        throw new TypeError("The selector must be a string identifying the targets.");
    } else if (texts === undefined || texts === null || texts.length === undefined) {
        throw new TypeError("The texts must be a valid string or list of strings.");
    } else if (isNaN(period) || period < 0) {
        throw new TypeError("The period must be a valid number.");
    }

    // Setup internal properties.
    this.elements  = document.querySelectorAll(selector);
    this.period    = period;
    this.iteration = 0;
    this.deleting  = false;
    this.current   = "";
    this.texts     = [];

    // Add classes to all elements
    Array.prototype.forEach.call(this.elements, function (element) {
        element.classList.add("typer");
    });

    // Setup text array as parameter "texts" must be parsed.
    if (texts instanceof String) {
        this.texts.push(texts);
    } else if (texts instanceof Array) {
        for (var i = 0; i < texts.length; i++) {
            this.texts.push(texts[i].toString());
        }
    } else {
        throw new TypeError("The texts must be either a String or an Array of strings.");
    }

    // Begin the text animation.
    this.tick();
}

/**
 * Tail recursive method that preforms the animation logic.
 * @method tick
 */
Textani.prototype.tick = function () {
    var that        = this;
    var textIndex   = this.iteration % this.texts.length;
    var currentText = this.texts[textIndex];

    // Calculate new text
    if (this.deleting) {
        this.current = currentText.substring(0, this.current.length - 1);
    } else {
        this.current = currentText.substring(0, this.current.length + 1);
    }

    // Iterate over each element and apply the given tick event.
    Array.prototype.forEach.call(that.elements, function (element) {
        // Update text
        element.innerHTML = "<span class=\"wrap\">" + that.current + "</span>";
    });

    // Check state and update counters
    var delay = 0;
    if (this.deleting && this.current === "") {
        this.deleting = false;
        this.iteration++;

        // wait five counts before begining to type again
        delay = this.period * 5;
    } else if (!this.deleting && this.current === currentText) {
        this.deleting = true;

        // wait half as long as it took to type before deleting
        delay = this.period * this.current.length / 2;
    } else {
        // just wait the normal amount
        delay = this.period;
    }

    // Wait for next tick
    setTimeout(function () {
        that.tick();
    }, delay);
};
