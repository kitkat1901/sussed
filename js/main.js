/*moving square to place given by co ordinates*/
function MoveSquareToMouse(x, y) {
	var div = document.getElementById("square");

	div.style.left = x + "px";
	div.style.top  = y + "px";
}

/*get co ordinates from mousemove event*/
function MouseHandler(event) {
	var x = event.pageX;
	var y = event.pageY;

	/*putting co-ordinates into log to check*/
	/*console.log("x = " + x + ", y = " + y);*/

	/*moving mouse to cordinates*/
	MoveSquareToMouse(x, y);
}

/*main is top of foodchain*/
function Main() {
	document.getElementsByTagName("body")[0].onmousemove = MouseHandler;

	var textani = new Textani("#textani", [
		"Is bees wax wrap meant to be so expensive?",
		"Is going Vegan actually that good for the planet?",
		"Surely it's impossible to buy snacks without plastic?"
	], 90);
}

/*make main master*/
window.addEventListener("DOMContentLoaded", Main);

